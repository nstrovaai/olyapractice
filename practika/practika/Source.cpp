#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <locale.h>
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#define SIZE 100
#define STR_SIZE 1024
#define KEY_SIZE 8

char database[SIZE] = "data.txt";
bool Availability_database = true;

int count_lake = 0;
FILE* fp_database = NULL;

struct specifications_lake //��������� ������������� ����� 
{
  int id; // ���������� ����� � �����
  int lake_name_length; // ����� ����� �����
  char* name_lake; // ��� �����
  int lenght_name_country; // ����� ����� ������
  char* name_country; // ��� ������
  int depth; // �������
  int lake_salinity; // ��������� �����

}lake; // ����������� ����������

void print_menu();

void add_note();

void copy_str(char*, char*, int*, int);

int read_str(char*);

int copy_to_mass_depth(int);

//�������� �������
void view_note(specifications_lake*, int);

// ������ �� �����
int read_from_file(FILE*, char*, specifications_lake*, int);

// ������ � ����
void save_in_file(FILE*, char*, specifications_lake*, int);

//������� ��������
specifications_lake* mass_lake = (specifications_lake*)calloc(1, sizeof(lake));
specifications_lake* mass_depth = (specifications_lake*)calloc(1, sizeof(lake));


int main()
{
  setlocale(LC_CTYPE, "rus");
  
  count_lake = read_from_file(fp_database, database, mass_lake, count_lake);

  bool not_error = true;
  int key_menu = 0;

  print_menu();
  
  // ���������� ���� ������
  while (1)
  {
    if (not_error)
    {
      printf_s("������� �������: ");
      scanf_s("%d", &key_menu);
      not_error = true;
    }  

    int count_note = 0;

    //����� ������
    switch (key_menu)
    {
    case 1:
      view_note(mass_lake, count_lake);
      break;
    case 2:
      add_note();
      break;
    case 3:      
      count_note = copy_to_mass_depth(count_lake);
      view_note(mass_depth, count_note);
      break;
    case 4:
      save_in_file(fp_database, database, mass_lake, count_lake);

      printf_s("��������� ��������� ���� ������\n");
      system("pause");

      return 0;
    default:
      printf_s("��� ����� �������\n");
      not_error = false;
    }
  }

  system("pause");

  return 0;
}
//�-� ������ ���� 
void print_menu()
{
  printf_s("��� ������������ ��������� ������ � ����� ������ ����!\n\n");
  printf_s("�������� ������� ���������:\n1 - �������� �������\n2 - ���������� ������");
  printf_s("\n3 - ����� �� ������� � ���������\n4 - ��������� ������\n\n");
}
//�-� ���������� ������ 
void add_note()
{
  
  lake.id = count_lake;
  getchar();

  char str_buf[SIZE] = "\0";
  int string_length = 0;

  printf_s("name lake:");
  string_length = read_str(str_buf);
  lake.name_lake = (char*)malloc(sizeof(char) * string_length + 1);
  copy_str(lake.name_lake, str_buf, &lake.lake_name_length, string_length);

  printf_s("name country: ");
  string_length = read_str(str_buf);
  lake.name_country = (char*)malloc(sizeof(char) * string_length + 1);
  copy_str(lake.name_country, str_buf, &lake.lenght_name_country, string_length);

  printf_s("depth: ");
  scanf_s("%d", &lake.depth);

  printf_s("lake_salinity: ");
  scanf_s("%d", &lake.lake_salinity);

  bool save_note = false;  

  // ������ � �����
  if (!save_note)
  {
    printf_s("���������� �������� � ����� ������ ��� ������� �������: %d\n", count_lake);

    mass_lake = (specifications_lake*)realloc(mass_lake, (count_lake + 1) * sizeof(lake));
    mass_lake[count_lake++] = lake;
  }
}

//�-� ������ �� �����
int read_from_file(FILE* fp, char* name_data, specifications_lake* mass, int count)
{
  fopen_s(&fp, name_data, "r");
  if (fp == NULL)
  {
    printf_s("�� ������� ������� ���� ����!\n���� ���� ������ ����� ������ ��� ���������� ������ ������!\n\n");
    Availability_database = false;
    return count;
  }

  // ������ ���������� ��������� � ��������� ������
  fread(&count, sizeof(int), 1, fp);

  mass = (specifications_lake*)realloc(mass, sizeof(lake) * count);

  mass_lake = mass;

  // ������ ������� �� ������
  for (int i = 0; i < count; i++)
  {
    fread(&mass[i].id, sizeof(int), 1, fp);
	fread(&mass[i].lake_name_length, sizeof(int), 1, fp);
    mass[i].name_lake = (char*)malloc(sizeof(char) * mass[i].lake_name_length + 1);
    fread(mass[i].name_lake, sizeof(char), mass[i].lake_name_length, fp);
    mass[i].name_lake[mass[i].lake_name_length] = '\0';

    fread(&mass[i].lenght_name_country, sizeof(int), 1, fp);
    mass[i].name_country = (char*)malloc(sizeof(char) * mass[i].lenght_name_country + 1);
    fread(mass[i].name_country, sizeof(char), mass[i].lenght_name_country, fp);
    mass[i].name_country[mass[i].lenght_name_country] = '\0';

    fread(&mass[i].depth, sizeof(int), 1, fp);

    fread(&mass[i].lake_salinity, sizeof(double), 1, fp);
  }

  fclose(fp);
  return count;

}

//�-� ��������� �������
void view_note(specifications_lake * mass, int count)
{
  setlocale(LC_CTYPE, "rus");
  // ���� �����
  if (count_lake == 0)
  {
    printf_s("���� ������ �����!\n");
    return;
  }

  printf_s("����� |       �����      |         ������        |     �������     |              ���������              |\n");

  // ����� ����
  for (int i = 0; i < count; i++)
  {    
    printf_s("%5d | %16s | %21s | %15d | %35d |\n", mass[i].id, mass[i].name_lake, mass[i].name_country, mass[i].depth, mass[i].lake_salinity);
  }
}

//�-� ���������� � ����
void save_in_file(FILE * fp, char* name_data, specifications_lake * mass, int count)
{
  fopen_s(&fp, name_data, "w+b");

  // ����������� �� ����
  if (!Availability_database)
  {
    printf_s("���� ���� ������ ������ � ������ %s\n", name_data);
  }

  fwrite(&count, sizeof(int), 1, fp);

  // ������ ������� ������ � �����
  for (int i = 0; i < count; i++)
  {
    fwrite(&mass[i].id, sizeof(int), 1, fp);

    fwrite(&mass[i].lake_name_length, sizeof(int), 1, fp);
    fwrite(mass[i].name_lake, sizeof(char), mass[i].lake_name_length, fp);

    fwrite(&mass[i].lenght_name_country, sizeof(int), 1, fp);
    fwrite(mass[i].name_country, sizeof(char), mass[i].lenght_name_country, fp);

    fwrite(&mass[i].depth, sizeof(int), 1, fp);

    fwrite(&mass[i].lake_salinity, sizeof(double), 1, fp);
  }

  fclose(fp);
}

//�-� ���������� ������ 
int read_str(char* str)
{
  gets_s(str, SIZE);
  return strlen(str);
}

//�-� ����������� ������ 
void copy_str(char* new_str, char* start_str, int* per_len, int str_len)
{
  strcpy(new_str, start_str);
  *per_len = str_len;
  new_str[str_len + 1] = '\0';
}

int copy_to_mass_depth(int count)
{
  count = 0;

  for (int i = 0; i < count_lake; i++)
  {
    if (mass_lake[i].depth < 50 && mass_lake[i].lake_salinity > 20)
    {
      mass_depth = (specifications_lake*)realloc(mass_depth, (count + 1) * sizeof(lake));

      mass_depth[count].id = mass_lake[i].id;

      mass_depth[count].lake_name_length = mass_lake[i].lake_name_length;
      mass_depth[count].name_lake = mass_lake[i].name_lake;

      mass_depth[count].lenght_name_country = mass_lake[i].lenght_name_country;
      mass_depth[count].name_country = mass_lake[i].name_country;

      mass_depth[count].depth = mass_lake[i].depth;
    
      mass_depth[count].lake_salinity = mass_lake[i].lake_salinity;

      count++;
    }
  }
  return count;
}